﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Hotal_Management
{
    public partial class Form1 : Form
    {
        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\awake\OneDrive\Desktop\Project Fire Database.accdb");
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            OleDbCommand cmd = new OleDbCommand("SELECT * from STUDENT", con);
            con.Open();
            OleDbDataReader rd = cmd.ExecuteReader();

            DataTable dt = new DataTable();

            dt.Load(rd);
            con.Close();
            dataGridView1.DataSource = dt;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            con.Open();

            OleDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " Select DetailID,StudentID,ChoreID,Date from ChoreDetail ORDER BY DetailID ASC";

            OleDbDataAdapter a = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            a.SelectCommand = cmd;
            a.Fill(dt);
            ViewChores.DataSource = dt;
            ViewChores.AutoResizeColumns();

            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            con.Open();

            OleDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " Select DetailID,StudentID,ChoreID,Date from ChoreDetail Where ChoreID IS NULL ORDER BY StudentID ASC";

            OleDbDataAdapter a = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            a.SelectCommand = cmd;
            a.Fill(dt);
            ViewChores.DataSource = dt;
            ViewChores.AutoResizeColumns();

            con.Close();
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            con.Open();
            //int number = textBox1.Text;

            OleDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            //cmd.CommandText = "Select * from Chores";
            cmd.CommandText = " Select * from STUDENT WHERE StudentID = @input";
            //can set sql inside to know id.

            cmd.Parameters.AddWithValue("@input", textBox2.Text);

            OleDbDataAdapter a = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            a.SelectCommand = cmd;
            a.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AutoResizeColumns();

            con.Close();
        }

        private void btnAnnounce_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Warning has been show to them");
        }

        private void button3_Click(object sender, EventArgs e)
        {
          //show insert to SQl already. 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbUpdate.Text = "EMAIL HAS BEEN SEND AND UPDATE THERE WORK";
        }
        private void FEEDBACK_CheckedChanged_1(object sender, EventArgs e)
        {
            if (FEEDBACK.Checked)
            {
                lbINFO2.Text = "CHECK PARTY";
                con.Open();
                OleDbCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = " Select * from Feedback";
                OleDbDataAdapter a = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                a.SelectCommand = cmd;
                a.Fill(dt);
                ViewChores.DataSource = dt;
                ViewChores.AutoResizeColumns();
                con.Close();
            }
        }

        private void GROCERIES_CheckedChanged(object sender, EventArgs e)
        {
            if (GROCERIES.Checked)
            {
                lbINFO2.Text = "GROCERIES";
                con.Open();
                OleDbCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = " Select * from GroceriesDetail";

                OleDbDataAdapter a = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                a.SelectCommand = cmd;
                a.Fill(dt);
                ViewChores.DataSource = dt;
                ViewChores.AutoResizeColumns();
                con.Close();
            }
        }
    }
}
